package itree.demo.controller;

import com.sise.itree.common.BaseController;
import com.sise.itree.common.annotation.ControllerMapping;
import com.sise.itree.core.handle.response.BaseResponse;
import com.sise.itree.model.ControllerRequest;

/**
 * @author idea
 * @data 2019/5/1
 */
@ControllerMapping(url = "/third-controller")
public class ThirdContoller implements BaseController {

    @Override
    public BaseResponse doGet(ControllerRequest controllerRequest) {
        String pwd= (String) controllerRequest.getParameter("password");
        System.out.println(pwd);
        return new BaseResponse(3,pwd);
    }

    @Override
    public BaseResponse doPost(ControllerRequest controllerRequest) {
        return null;
    }
}
