package itree.demo.filter;


import com.sise.itree.common.BaseFilter;
import com.sise.itree.common.annotation.Filter;
import com.sise.itree.model.ControllerRequest;

/**
 * @author idea
 * @data 2019/5/1
 */
@Filter(order = 3)
public class ThirdFilter implements BaseFilter {

    @Override
    public void beforeFilter(ControllerRequest controllerRequest) {
        System.out.println("this is 3 before");
    }

    @Override
    public void afterFilter(ControllerRequest controllerRequest) {
        System.out.println("this is 3 after");
    }
}
