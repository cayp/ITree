package itree.demo.filter;

import com.sise.itree.common.BaseFilter;
import com.sise.itree.common.annotation.Filter;
import com.sise.itree.model.ControllerRequest;

/**
 * @author idea
 * @data 2019/4/30
 */
@Filter(order = 1)
public class MyFilter implements BaseFilter {

    @Override
    public void beforeFilter(ControllerRequest controllerRequest) {
        System.out.println("before");
    }

    @Override
    public void afterFilter(ControllerRequest controllerRequest) {
        System.out.println("after");
    }
}
